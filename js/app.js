require.config({
    urlArgs : 'r=' + (new Date()).getTime(),
    paths : {
        underscore : ['libs/underscore'],
        backbone : ['libs/backbone'],
        jquery : ['libs/jquery']
    },
    shim : {
        'backbone' : {
            deps: ['underscore', 'jquery']
        },
        'module/table' : {
            deps: ['backbone']
        },
        'module/button' : {
            deps: ['backbone']
        },
        'module/paginator' : {
            deps: ['backbone']
        },
        'module/search' : {
            deps: ['backbone']
        }
    }
});

require([
    'module/table',
    'module/button',
    'module/search',
    'module/paginator'
], function(Table, Button, Search, Paginator) {

    var createButton = new Button({
        $el : $('#create-btn')
    });

    var editButton = new Button({
        $el : $('#edit-btn'),
        disabled : true
    });

    var removeButton = new Button({
        $el : $('#remove-btn'),
        disabled : true
    });

    var submitButton = new Button({
        $el : $('#submit-btn'),
        disabled : true
    });

    var resetButton = new Button({
        $el : $('#reset-btn'),
        disabled : true
    });

    var documents = new Backbone.Collection();

    var table = new Table({
        collection : documents,
        $el : $('#documents')
    });

    var search = new Search({
        $el : $('#search')
    });

    table.listenTo(createButton, 'click', table.showCreateForm);
    table.listenTo(editButton, 'click', table.showEditForm);
    table.listenTo(removeButton, 'click', table.removeRows);
    table.listenTo(submitButton, 'click', table.update);
    table.listenTo(resetButton, 'click', table.hideForm);
    table.listenTo(search, 'search', table.search);
    table.listenTo(search, 'reset', table.resetSearch);

    createButton.listenTo(createButton, 'click', createButton.disable);
    createButton.listenTo(editButton, 'click', createButton.disable);
    createButton.listenTo(submitButton, 'click', createButton.enable);
    createButton.listenTo(resetButton, 'click', createButton.enable);
    createButton.listenTo(table, 'chooseRows', createButton.disable);
    createButton.listenTo(table, 'unchooseRows', createButton.enable);

    editButton.listenTo(createButton, 'click', editButton.disable);
    editButton.listenTo(editButton, 'click', editButton.disable);
    editButton.listenTo(submitButton, 'click', editButton.disable);
    editButton.listenTo(table, 'chooseRows', editButton.enable);
    editButton.listenTo(table, 'unchooseRows', editButton.disable);

    removeButton.listenTo(editButton, 'click', removeButton.disable);
    removeButton.listenTo(table, 'chooseRows', removeButton.enable);
    removeButton.listenTo(table, 'unchooseRows', removeButton.disable);

    submitButton.listenTo(createButton, 'click', submitButton.enable);
    submitButton.listenTo(editButton, 'click', submitButton.enable);
    submitButton.listenTo(submitButton, 'click', submitButton.disable);
    submitButton.listenTo(resetButton, 'click', submitButton.disable);

    resetButton.listenTo(createButton, 'click', submitButton.enable);
    resetButton.listenTo(editButton, 'click', submitButton.enable);
    resetButton.listenTo(submitButton, 'click', submitButton.disable);
    resetButton.listenTo(resetButton, 'click', submitButton.disable);

    search.listenTo(createButton, 'click', search.disable);
    search.listenTo(editButton, 'click', search.disable);
    search.listenTo(submitButton, 'click', search.enable);
    search.listenTo(resetButton, 'click', search.enable);

    var DocumentBuilder = Backbone.Model.extend({
        build : function() {
            var start = this.get('start');
            var limit = start + this.get('limit');
            for (;start < limit;++start) {
                this.get('collection').add({
                    code : start,
                    name : 'Документ#' + start
                });
            }
            this.set('start', start);
        }
    });

    var paginator = new Paginator();

    var builder = new DocumentBuilder({
        start : 0,
        limit : 100,
        collection : documents
    });

    builder.listenTo(paginator, 'fetch', builder.build);

    builder.build();
});