define(function() {
    return Backbone.View.extend({
        initialize : function() {
            var $this = this;
            $this.options.disabled = $this.options.disabled || false;
            $this.options.$el.bind('click', function(event) {
                $this.trigger('click', event, $this);
            });
            $this.render();
        },
        render : function() {
            this.options.$el.attr({
                disabled : this.options.disabled
            });
        },
        enable : function() {
            this.options.disabled = false;
            this.render();
        },
        disable : function() {
            this.options.disabled = true;
            this.render();
        }
    });
});