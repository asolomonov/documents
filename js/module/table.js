define(function() {
    var Row = Backbone.View.extend({
        initialize : function() {
            this.options.appendMethod = this.options.appendMethod || 'appendTo';
            this.listenTo(this.model, 'change', this.change);
            this.$el = $(_.template(this.options.template)({
                document : this.model
            }))[this.options.appendMethod](this.options.tbody);
            this.initEvents();
        },
        initEvents : function() {
            var $this = this;
            $this.$el.find('[type=checkbox]').bind('change', function(event) {
                if ($(event.target).prop('checked')) {
                    $this.model.set('choosed', true);
                    $this.trigger('choose', event, $this);
                } else {
                    $this.model.unset('choosed');
                    $this.trigger('unchoose', event, $this);
                }
            });
        },
        render : function() {
            this.$el.replaceWith(_.template(this.options.template)({
                document : this.model
            }));
            this.$el = $('#row-' + this.model.cid);
            this.initEvents();
        },
        create : function(table) {
            table.collection.add({
                code : this.$el.find('[name=code]').val(),
                name : this.$el.find('[name=name]').val()
            });
        },
        edit : function(table) {
            if (this.model.get('editing')) {
                this.options.template = table.rowTemplate;
                this.model.unset('editing');
                this.model.unset('disable');
                this.model.set('code', this.$el.find('[name=code]').val());
                this.model.set('name', this.$el.find('[name=name]').val());
                this.model.set('edited', true);
            }
        },
        reset : function(table) {
            if (this.model.get('editing')) {
                this.options.template = table.rowTemplate;
                this.model.unset('editing');
                this.model.unset('disable');
                this.$el.change();
                this.model.set('edited', true);
            } else if (this.model.get('creating')) {
                table.collection.remove(this.model);
            }
        },
        change : function(model) {
            if (model.get('editing')) {
                this.render();
                this.$el.find('[type=checkbox]')
                    .attr('checked', true)
                    .change();
            } else if (model.get('edited')) {
                this.model.unset('edited');
                this.render();
                this.$el.find('[type=checkbox]')
                    .attr('checked', false)
                    .change();
                this.trigger('editSuccess', this);
            }
            if (model.get('disable')) {
                this.$el.find('[type=checkbox]')
                    .attr('disabled', true);
            } else {
                this.$el.find('[type=checkbox]')
                    .attr('disabled', false);
            }
        },
        onRowsEditSuccess : function(table) {
            if (this.model.get('disable')) {
                this.model.unset('disable')
            }
        },
        onCheckAll : function(event, table) {
            this.$el.find('[type=checkbox]')
                .attr('checked', !$(event.target).prop('checked'))
                .click();
        },
        remove : function(document) {
            this.$el.remove();
        }
    });

    return Backbone.View.extend({
        initialize : function() {
            this.tbody = this.options.$el.find('tbody');
            this.rowTemplate = $('#row-tmpl').html();
            this.errorRowTemplate = $('#error-row-tmpl').html();
            this.editRowTemplate = $('#edit-row-tmpl').html();
            this.rows = {};
            this.listenTo(this.collection, 'add', this.onCreateRow);
            this.listenTo(this.collection, 'remove', this.onRemoveRow);
            var $this = this;
            this.selectAll = this.options.$el.find('#select-all-btn');
            this.selectAll.bind('click', function(event) {
                $this.trigger('checkAll', event, $this);
            });
            this.orderBtn = this.options.$el.find('thead .order');
            this.orderBtn.bind('click', function(event) {
                event.preventDefault();
                $this.sort($(this).attr('prop'), parseInt($(this).attr('direct')));
            });
        },
        onCreateRow : function(model) {
            if (model.get('creating')) {
                var row = this.createRow({
                    template : this.editRowTemplate,
                    appendMethod : 'prependTo',
                    model : model
                });
                row.listenToOnce(this, 'createRow', row.create);
            } else if (model.get('error')) {
                this.createRow({
                    template : this.errorRowTemplate
                });
            } else {
                this.createRow({
                    model : model
                });
            }
        },
        createRow : function(config) {
            config = _.extend({
                table : this.options.$el,
                tbody : this.tbody,
                template : this.rowTemplate,
                model : new Backbone.Model()
            }, config);
            this.rows[config.model.cid] = new Row(config);
            this.listenTo(this.rows[config.model.cid], 'choose', this.onChooseRow);
            this.listenTo(this.rows[config.model.cid], 'unchoose', this.onUnchooseRow);
            this.listenTo(this.rows[config.model.cid], 'editSuccess', this.onEditSuccess);
            this.rows[config.model.cid].listenTo(this, 'resetRow', this.rows[config.model.cid].reset);
            this.rows[config.model.cid].listenTo(this, 'editRow', this.rows[config.model.cid].edit);
            this.rows[config.model.cid].listenTo(this, 'checkAll', this.rows[config.model.cid].onCheckAll);
            this.rows[config.model.cid].listenTo(this, 'rowsEditSuccess', this.rows[config.model.cid].onRowsEditSuccess);
            return this.rows[config.model.cid];
        },
        onChooseRow  :function(event, row) {
            if (1 == this.collection.where({choosed : true}).length) {
                this.trigger('chooseRows', event, row);
            }
        },
        onUnchooseRow : function(event, row) {
            if (0 == this.collection.where({choosed : true}).length) {
                this.trigger('unchooseRows', event, row);
            }
        },
        onRemoveRow : function(model) {
            if (this.rows[model.cid]) {
                this.rows[model.cid].remove();
                delete this.rows[model.cid];
            }
        },
        showCreateForm : function(event, button) {
            this.disableControlls();
            this.collection.add({
                creating : true
            });
            this.checkRows();
        },
        showEditForm : function(event, button) {
            this.disableControlls();
            var choosedRows = this.collection.where({choosed : true});
            if (choosedRows.length) {
                var $this = this;
                this.collection.forEach(function(model) {
                    if (-1 != _.indexOf(choosedRows, model)) {
                        $this.rows[model.cid].options.template = $this.editRowTemplate;
                        model.set('editing', true);
                    } else {
                        model.set('disable', true);
                    }
                });
            }
        },
        update : function() {
            this.enableControlls();
            if (null != (model = this.collection.findWhere({creating : true}))) {
                this.trigger('createRow', this);
                this.collection.remove(model);
            } else if (0 != (models = this.collection.where({editing : true})).length) {
                this.trigger('editRow', this);
            }
        },
        hideForm : function() {
            this.enableControlls();
            if (null != (model = this.collection.findWhere({creating : true}))) {
                this.off('createRow');
            }
            this.trigger('resetRow', this);
            this.checkRows();
        },
        removeRows : function(event, button) {
            var choosedRows = this.collection.where({choosed : true});
            if (choosedRows.length) {
                var $this = this;
                choosedRows.forEach(function(model) {
                    $this.collection.remove(model);
                });
                this.onUnchooseRow();
                if (this.isEmpty()) {
                    this.uncheckSelectAll();
                }
                this.checkRows();
            }
        },
        search : function(event, data) {
            var pattern = new RegExp(
                data.query.replace(/[^\w\dа-яА-Я]/g, '|')
            );
            this.clear();
            var $this = this;
            this.collection.forEach(function(model) {
                if (pattern.test(model.get('name'))) {
                    $this.onCreateRow(model);
                }
            });
            this.uncheckSelectAll();
            this.checkRows();
        },
        resetSearch : function() {
            this.clear();
            var $this = this;
            this.collection.forEach(function(model) {
                $this.onCreateRow(model);
            });
            this.uncheckSelectAll();
            this.checkRows();
        },
        clear : function() {
            this.collection.remove(
                this.collection.findWhere({error : true})
            );
            this.tbody.empty();
        },
        checkRows : function() {
            if (this.isEmpty()) {
                if (0 == this.collection.where({error : true}).length) {
                    this.collection.add({
                        error : true
                    });
                }
            } else if (!this.isEmpty() && null != (model = this.collection.findWhere({error : true}))) {
                this.collection.remove(model);
                this.tbody.find('#error-row').remove();
            }
        },
        isEmpty : function() {
            return 0 == this.tbody.children().not('#error-row').length;
        },
        uncheckSelectAll : function() {
            this.selectAll.attr('checked', false);
        },
        onEditSuccess : function() {
            if (0 == this.collection.where({edited : true}).length) {
                this.trigger('rowsEditSuccess', this);
            }
        },
        sort : function(property, direction) {
            direction = direction || 0;
            this.collection.comparator = function(a, b) {
                a = a.get(property);
                b = b.get(property);
                if (direction) {
                    if(a < b) {
                        return 1;
                    } else {
                        return -1;
                    }
                } else {
                    if(a > b) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
                return 0;
            };
            this.collection.sort();
            var models = this.collection.toArray();
            this.collection = this.collection.reset();
            this.clear();
            this.collection.add(models);
        },
        disableControlls : function() {
            this.orderBtn.add(this.selectAll).attr('disabled', true);
        },
        enableControlls : function() {
            this.orderBtn.add(this.selectAll).attr('disabled', false);
        }
    });
});