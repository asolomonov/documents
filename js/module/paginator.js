define(function() {
    return Backbone.Model.extend({
        initialize : function() {
            var $this = this;
            $(window).scroll(function() {
                if ($(window).scrollTop() + $(window).height() * 2 > $(document).height()) {
                    $this.trigger('fetch');
                }
            });
        }
    });
});