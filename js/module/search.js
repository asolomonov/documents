define(['module/button'], function(Button) {
    return Backbone.View.extend({
        initialize : function() {
            this.input = this.options.$el.find('input[type=text]');
            this.findButton = new Button({
                $el : this.options.$el.find('.btn.find')
            });
            this.resetButton = new Button({
                $el : this.options.$el.find('.btn.reset')
            });
            this.listenTo(this.findButton, 'click', function(event, button) {
                this.trigger('search', event, {
                    view : this,
                    button : button,
                    query : this.input.val()
                })
            });
            this.listenTo(this.resetButton, 'click', function(event, button) {
                this.input.val('');
                this.trigger('reset', event, button)
            });
        },
        disable : function() {
            this.findButton.disable();
            this.resetButton.disable();
        },
        enable : function() {
            this.findButton.enable();
            this.resetButton.enable();
        }
    });
});